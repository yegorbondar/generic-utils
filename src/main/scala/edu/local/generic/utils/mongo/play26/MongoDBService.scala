package edu.local.generic.utils.mongo.play26

import play.modules.reactivemongo.{ReactiveMongoApi, ReactiveMongoComponents}


case class MongoDBService(reactiveMongoApi: ReactiveMongoApi) extends ReactiveMongoComponents
  with BaseMongoDBService
