package edu.local.generic.utils.mongo

import play.api.libs.json.{Json, OFormat}

case class Person(name: String, location: String)

object Person {

  val defaultPerson = Person("test_name", "test_location")

  def findByName(name: String) = Json.obj("name" -> name)


  implicit val personEntityContext = new EntityContext[Person] {
    override def collectionName: String = "persons"
  }

  implicit val personFormat: OFormat[Person] = Json.format[Person]
}
