package edu.local.generic.utils.mongo.play26

import cats.data.Xor.{left, right}
import cats.data.{Xor, XorT}
import cats.implicits._
import edu.local.generic.utils.model.BaseError
import edu.local.generic.utils.mongo.{EntityContext, MongoNotFoundError, MongoThrowableError, MongoWriteError}
import play.api.libs.json.{JsObject, OWrites, Reads}
import play.modules.reactivemongo.ReactiveMongoComponents
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.{Cursor, DefaultDB}
import reactivemongo.play.json.collection.JSONCollection

import scala.concurrent.{ExecutionContext, Future}
import scala.language.higherKinds
import scala.util.control.NonFatal

trait BaseMongoDBService {
  self: ReactiveMongoComponents =>

  import reactivemongo.play.json._

  type MongoResult[X] = BaseMongoDBService.MongoResult[X]
  type MongoMaybeError = BaseMongoDBService.MongoMaybeError
  type MongoResultT[F[_], X] = BaseMongoDBService.MongoResultT[F, X]
  type FMongoResult[X] = BaseMongoDBService.FMongoResult[X]
  type FMongoMaybeError = BaseMongoDBService.FMongoMaybeError

  final def persist[T: OWrites](obj: T)(implicit context: EntityContext[T], ec: ExecutionContext): FMongoMaybeError =
    withCollection(_.insert(obj)) subflatMap writeResultTransformer

  final def remove[T: OWrites](query: JsObject)(implicit context: EntityContext[T], ec: ExecutionContext): FMongoMaybeError =
    withCollection(_.remove(query)) subflatMap writeResultTransformer

  final def find[T: Reads](query: JsObject)(implicit context: EntityContext[T], ec: ExecutionContext): FMongoResult[List[T]] =
    withCollection(_.find(query).cursor[T]().collect[List](-1, Cursor.FailOnError[List[T]]()))

  final def singleResult[T: Reads](query: JsObject)(implicit context: EntityContext[T], ec: ExecutionContext): FMongoResult[T] =

    withCollection(_.find(query).cursor[T]().headOption) subflatMap {
      case Some(entity) => right(entity)
      case None => left(MongoNotFoundError(context.collectionName, query))
    }

  protected final def withCollection[X, R](f: JSONCollection => Future[R])(implicit context: EntityContext[X], ec: ExecutionContext): FMongoResult[R] =
    withDB { db => f apply db.collection(context.collectionName) }

  protected final def withDB[X](f: DefaultDB => Future[X])(implicit ec: ExecutionContext): FMongoResult[X] =
    XorT apply reactiveMongoApi.database
      .flatMap(f)
      .map(right)
      .recover { case NonFatal(e) => left(MongoThrowableError(e)) }

  protected val writeResultTransformer: WriteResult => Xor[BaseError, Unit] = {
    case result if !result.ok => left(MongoWriteError(result.toString, result.writeErrors))
    case _ => right(Unit)
  }

}

object BaseMongoDBService {

  type MongoResult[X] = Xor[BaseError, X]

  type MongoMaybeError = Xor[BaseError, Unit]

  type MongoResultT[F[_], X] = XorT[F, BaseError, X]

  type FMongoResult[X] = MongoResultT[Future, X]

  type FMongoMaybeError = FMongoResult[Unit]

}
