package edu.local.generic.utils.mongo

import edu.local.generic.utils.model.BaseError
import play.api.libs.json.JsObject
import reactivemongo.api.commands.WriteError

sealed abstract class MongoError(message: String, cause: Option[Throwable] = None) extends BaseError(message, cause)

case class MongoThrowableError(exception: Throwable) extends MongoError(exception.getMessage, Some(exception))

case class MongoWriteError(msg: String, errors: Seq[WriteError]) extends MongoError(msg)

case class MongoNotFoundError(collection: String, query: JsObject) extends MongoError(s"Nothing found in collection [$collection] for query [$query]")
