## Overview

The set of generic components for different Scala frameworks that has proof in the real, production
systems.

### Generic ReactiveMongo integration for Play Framework

[ReactiveMongo](http://reactivemongo.org/) provides good async API to work with MongoDB.
But if you have a lot of entities to store in MongoDB, the amount of `DAO`s code duplication grows in linear progression.
In order to avoid such issue the `Generic` `MongoDBService` was created.

**Note**: `BaseMongoDBService` uses cake pattern and requires `reactivemongo-play` dependency.

The `generic` concept is pretty simple:
    
1. Type class `EntityContext` provides `collection` information for certain entity;
2. Each entity should provide `play.json.OFormat` JSON decoder and encoder pair;
3. Service returns right biased `Either` implementation from `cats` library: [Xor](http://eed3si9n.com/herding-cats/Xor.html).

