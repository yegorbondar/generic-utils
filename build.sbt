lazy val generic_utils = (project in file(".")).
  enablePlugins(AssemblyPlugin, ReleasePlugin).
  settings(

    organization := "edu.local.generic",
    name := "utils",
    version := "0.0.1",
    scalaVersion := "2.11.8",

    credentials += Credentials(Path.userHome / ".sbt" / "credentials"),

    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case _ => MergeStrategy.first
    },

    addCompilerPlugin(
      "org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full
    ),

    libraryDependencies ++= Seq(
      "org.reactivemongo" %% "play2-reactivemongo" % "0.13.0-play26" % Provided,
      "org.reactivemongo" %% "reactivemongo-play-json" % "0.13.0-play26" % Provided,
      "com.typesafe.play" %% "play-json" % "2.6.7" % Provided,
      "org.typelevel" %% "cats" % "0.7.0",
      "org.scalactic" %% "scalactic" % "3.0.0"
    ),

    //logging
    libraryDependencies ++= Seq(
      "org.slf4j" % "slf4j-api" % "1.7.25"
    ),

    //Test
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.0.0" % Test,
      "org.scalacheck" %% "scalacheck" % "1.13.4" % Test,
      "de.flapdoodle.embed" % "de.flapdoodle.embed.mongo" % "2.0.3" % Test,
      "com.github.simplyscala" %% "scalatest-embedmongo" % "0.2.4" % Test,
      "org.scalamock" %% "scalamock-scalatest-support" % "3.3.0" % Test,
      "org.mockito" % "mockito-core" % "1.10.19" % Test
    ),

    fork in Test := false,
    parallelExecution in Test := false,
    javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")
  )
