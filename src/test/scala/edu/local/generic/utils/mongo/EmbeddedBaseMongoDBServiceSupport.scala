package edu.local.generic.utils.mongo

import com.github.simplyscala.{MongoEmbedDatabase, MongodProps}
import de.flapdoodle.embed.process.runtime.Network
import edu.local.generic.utils.mongo.play26.BaseMongoDBService
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, Suite}
import play.modules.reactivemongo.{ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.MongoConnection.ParsedURI
import reactivemongo.api.{DefaultDB, MongoConnectionOptions, MongoDriver}
import reactivemongo.play.json.collection.JSONCollection

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait EmbeddedBaseMongoDBServiceSupport[X] extends MongoEmbedDatabase with BeforeAndAfterAll with MockitoSugar {
  self: Suite =>

  protected var mongoProps: MongodProps = _
  protected var baseMongoDBService: BaseMongoDBService = _
  protected var port: Int = _

  def entityContext: EntityContext[X]

  @tailrec
  private def availablePort(): Int = {
    val freePort = Network.getFreeServerPort
    if (freePort > 0) freePort else availablePort()
  }

  override protected def beforeAll(): Unit = {
    port = availablePort()
    mongoProps = mongoStart(port)

    val db = mockDB()
    val api = mockApi(db)
    val contextCollection = mockContextCollection(db)

    doReturn(contextCollection).when(db).collection[JSONCollection](anyString(), anyObject())(anyObject())

    baseMongoDBService = new BaseMongoDBService with ReactiveMongoComponents {
      override def reactiveMongoApi: ReactiveMongoApi = api
    }
  }

  override protected def afterAll(): Unit = {
    mongoStop(mongoProps)
  }

  private def mockDB(): DefaultDB = {
    val driver = new MongoDriver
    val connection = driver.connection(ParsedURI(
      hosts = List(("localhost", port)),
      options = MongoConnectionOptions(nbChannelsPerNode = 1),
      ignoredOptions = List.empty[String],
      None,
      None
    ))

    spy(connection("test" + port))
  }

  private def mockApi(db: DefaultDB): ReactiveMongoApi = {
    val api = mock[ReactiveMongoApi]

    doReturn(Future successful db).when(api).database
    doReturn(db).when(api).db
    doReturn(Future.successful(db)).when(api).database
    doReturn(db.connection).when(api).connection

    api
  }

  private def mockContextCollection(db: DefaultDB): JSONCollection = {
//    val contextCollection = spy(db.collection[JSONCollection](entityContext.collectionName))
    val contextCollection = db.collection[JSONCollection](entityContext.collectionName)

//    doReturn(Future successful DefaultWriteResult(ok = false, 0, Nil, None, None, None))
//      .when(contextCollection).remove[X](anyObject(), anyObject(), anyObject())(anyObject(), anyObject())

    contextCollection
  }

}
