package edu.local.generic.utils.mongo

import java.util.concurrent.TimeUnit

import com.github.simplyscala.{MongoEmbedDatabase, MongodProps}
import de.flapdoodle.embed.process.runtime.Network
import edu.local.generic.utils.mongo.play26.MongoDBService
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, Suite}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.MongoConnection.ParsedURI
import reactivemongo.api.{MongoConnectionOptions, MongoDriver}

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


trait EmbeddedMongoSupport extends MongoEmbedDatabase with BeforeAndAfterAll {
  self: Suite =>

  protected var mongoProps: MongodProps = _
  protected var mongoDBService: MongoDBService = _
  protected var port: Int = _

  @tailrec
  private def getAvailablePort(): Int = {
    val freePort = Network.getFreeServerPort
    if (freePort > 0) freePort else getAvailablePort()
  }

  override protected def beforeAll(): Unit = {
    port = getAvailablePort()
    mongoProps = mongoStart(port)

    val driver = new MongoDriver
    val connection = driver.connection(ParsedURI(
      hosts = List(("localhost", port)),
      options = MongoConnectionOptions(nbChannelsPerNode = 1),
      ignoredOptions = List.empty[String],
      None,
      None
    ))

    val db = connection("test" + port)
    val api = mock(classOf[ReactiveMongoApi])

    when(api.database).thenReturn(Future.successful(db))
    doReturn(db).when(api).db
    doReturn(Future.successful(db)).when(api).database
    doReturn(connection).when(api).connection

    mongoDBService = new MongoDBService(api)

    TimeUnit.SECONDS.sleep(10)
  }

  override protected def afterAll(): Unit = {
    mongoStop(mongoProps)
  }
}
