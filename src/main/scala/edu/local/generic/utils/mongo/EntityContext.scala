package edu.local.generic.utils.mongo


trait EntityContext[T] {
  def collectionName: String
}
