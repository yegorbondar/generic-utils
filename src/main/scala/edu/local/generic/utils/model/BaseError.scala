package edu.local.generic.utils.model

abstract class BaseError(message: String, cause: Option[Throwable]) {
  final def getMessage: String = message

  final def getCause: Option[Throwable] = cause
}

case class ThrowableError(error: Throwable) extends BaseError(error.getMessage, Some(error))
