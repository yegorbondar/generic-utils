package edu.local.generic.utils.mongo

import cats.data.Xor
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}
import play.api.libs.json.Json
import org.scalatest.Inside._
import cats.implicits._
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


class BaseMongoDBServiceTest extends FlatSpec with Matchers with GivenWhenThen with EmbeddedBaseMongoDBServiceSupport[Person] {

  override def entityContext: EntityContext[Person] = Person.personEntityContext

  private val defaultDuration = 10.seconds

  "BaseMongoDBService" should "return MongoNotFoundError if the requested entity is not exist" in {

    val result = Await.result(
      baseMongoDBService.singleResult[Person](Json.obj("randomField" -> "randomValue")).value,
      defaultDuration
    )

    inside(result) {
      case Xor.Left(x) => x shouldBe an[MongoNotFoundError]
    }
  }

  it should "write and read entity" in {
    val operations = for {
      _ <- baseMongoDBService.persist[Person](Person.defaultPerson)
      r <- baseMongoDBService.singleResult[Person](Person.findByName(Person.defaultPerson.name))
    } yield r

    inside(Await.result(operations.value, defaultDuration)) {
      case Xor.Right(v) => v shouldBe Person.defaultPerson
    }
  }

  it should "return non empty collection of saved entities" in {
    val operations = for {
      _ <- baseMongoDBService.persist[Person](Person.defaultPerson)
      entities <- baseMongoDBService.find[Person](Json.obj())
    } yield entities

    inside(Await.result(operations.value, defaultDuration)) {
      case Xor.Right(entities) => entities should not be empty
    }
  }

}
